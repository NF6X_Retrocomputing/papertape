########################################################################
# Copyright (C) 2014 Mark J. Blair, NF6X
#
# This file is part of papertape.
#
#  papertape is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  papertape is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with papertape.  If not, see <http:#www.gnu.org/licenses/>.
########################################################################

"""ASCII/TTY coding conversion data."""

# Constants for asc2tty array
INVC	= 0o377		# invalid character mapping
FIGS_F 	= 0o200		# figures required flag
ETHR_F	= 0o100		# valid in either shift
LTRS	= 0o037		# letters shift character
FIGS	= 0o033		# figures shift character
MSK5	= 0o037		# mask off 5 LSBs
MSK7	= 0o177		# mask off 7 LSBs


# Array for converting ASCII to 5-level TTY code.
#
# 5 LSBs are significant.
# Bit 7 set if figures shift required.
# 0xff indicates invalid character mapping.
# Lower-case mapped to upper-case
# See also:
# http://homepages.cwi.nl/~dik/english/codes/5tape.html#teletype
# ascii(7)
#
asc2tty = [
    0o100, INVC, INVC, INVC, INVC, INVC, INVC, 0o233, # NUL, ..., BEL
    INVC, INVC, 0o110, INVC, INVC, 0o102, INVC, INVC, # ..., LF, ..., CR
    INVC, INVC, INVC, INVC, INVC, INVC, INVC, INVC, # ...
    INVC, INVC, INVC, INVC, INVC, INVC, INVC, INVC, # ...

    0o104, 0o226, 0o221, 0o205, 0o222, INVC, 0o213, 0o224, # SP, punct.
    0o236, 0o211, INVC, INVC, 0o206, 0o230, 0o207, 0o227, # punct.
    0o215, 0o235, 0o231, 0o220, 0o212, 0o201, 0o225, 0o234, # 0-7
    0o214, 0o203, 0o216, 0o217, INVC, INVC, INVC, 0o223, # 8-9, punct

    INVC, 0o030, 0o023, 0o016, 0o022, 0o020, 0o026, 0o013, # @, A-G
    0o005, 0o014, 0o032, 0o036, 0o011, 0o007, 0o006, 0o003, # H-O
    0o015, 0o035, 0o012, 0o024, 0o001, 0o034, 0o017, 0o031, # P-W
    0o027, 0o025, 0o021, INVC, INVC, INVC, INVC, INVC, # X-Z, punct

    INVC, 0o030, 0o023, 0o016, 0o022, 0o020, 0o026, 0o013, # ', a-g
    0o005, 0o014, 0o032, 0o036, 0o011, 0o007, 0o006, 0o003, # h-o
    0o015, 0o035, 0o012, 0o024, 0o001, 0o034, 0o017, 0o031, # p-w
    0o027, 0o025, 0o021, INVC, INVC, INVC, INVC, INVC  # x-z, punct, DEL
]


# Arrays for converting 5-level TTY code to ASCII.

tty_ltrs2asc = [
    '\x00', 'T', '\x0D', 'O',  ' ', 'H', 'N', 'M',
    '\x0A', 'L', 'R',    'G',  'I', 'P', 'C', 'V',
    'E',    'Z', 'D',    'B',  'S', 'Y', 'F', 'X',
    'A',    'W', 'J',    FIGS, 'U', 'Q', 'K', LTRS]

tty_figs2asc = [
    '\x00', '5', '\x0D', '9',  ' ', '#', ',', '.',
    '\x0A', ')', '4',    '&',  '8', '0', ':', ';',
    '3',    '"', '$',    '?',  "'", '6', '!', '/',
    '-',    '2', '\x07', FIGS, '7', '1', '(', LTRS]

