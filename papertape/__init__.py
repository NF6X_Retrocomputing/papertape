########################################################################
# Copyright (C) 2014 Mark J. Blair, NF6X
#
# This file is part of papertape.
#
#  papertape is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  papertape is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with papertape.  If not, see <http://www.gnu.org/licenses/>.
########################################################################

"""Provides support for manipulating images of punched paper tapes."""

__all__       = ['tape', 'font5x7', 'pbmtape', 'tty']
__version__   = '2.2'
__copyright__ = 'Copyright (C) 2014-2020 Mark J. Blair, released under GPLv3'
__pkg_url__   = 'http://www.nf6x.net/tags/papertape/'
__dl_url__    = 'https://gitlab.com/NF6X_Retrocomputing/papertape'


from .tape import *
